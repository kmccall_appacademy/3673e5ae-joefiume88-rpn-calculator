class RPNCalculator
  # TODO: your code goes here!

  attr_accessor :values

  def initialize
    @values = []
  end

  def value
    @values.last
  end

  def error
    "calculator is empty"
  end

  def plus
    raise error if values.size < 1
    @values[-2] = @values[-2..-1].reduce(:+)
    @values.pop
    @values
  end

  def minus
    raise error if values.size < 1
    @values[-2] = @values[-2..-1].reduce(:-)
    @values.pop
    @values
  end

  def times
    raise error if values.size < 1
    @values[-2] = @values[-2..-1].map! {|el| el.to_f}.reduce(:*)
    @values.pop
    @values
  end

  def divide
    raise error if values.size < 1
    @values[-2] = @values[-2..-1].map! {|el| el.to_f}.reduce(:/)
    @values.pop
    @values
  end

  def tokens(str)
    arr = []
    operations = '*+-/'

    str.each_char do |char|
      next if char == ' '

      if operations.include?(char)
        arr << char.to_sym
      else
        arr << char.to_i
      end
    end

    arr
  end

  def evaluate(str)
    str.each_char do |char|
      next if char == ' '

      if char == '*'
        times
      elsif char == '/'
        divide
      elsif char == '+'
        plus
      elsif char == '-'
        minus
      else
        @values << char.to_i
      end
    end

    value
  end

  def push(num)
    @values.push(num)
  end
end
